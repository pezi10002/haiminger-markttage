# Haiminger Markttage


Ziel unseres Projekts ist es bis 1. Mai 2021 eine Mobile-App, sowie eine Website 
für die „Haiminger Markttage“ zu erstellen, welche es den Besuchern des Markts 
erleichtern soll Produkte und Aussteller zu finden. 

Die Mobile App soll im Oktober 2021 zum ersten Mal für dieses dreitägige Event 
zum Einsatz kommen und bis 30. Juli 2021 als Download für Android- und IOS-Geräte 
zur Verfügung stehen. 

Dabei ist uns wichtig die Mobile-App und die mobile Ansicht der Website sehr schlank 
und übersichtlich zu gestalten, um die App möglichst nutzerfreundlich für 
Jung und Alt zu machen. 

Neben der Suchfunktion für Aussteller und Produkte, sollen Funktionen wie 
Anfahrtsmöglichkeiten inklusive Google-Maps Schnittstelle, eine Übersichtskarte 
des Markts, eine News-Rubrik, in welcher die Nutzer am Laufenden gehalten werden
sollen, sowie Infos über den Shuttle Service, dem öffentlichen WC und der Position 
der Sanitäter am Markt gegeben sein.
